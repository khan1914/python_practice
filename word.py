

with open("word.text", mode="r") as read_file:
    words_all = []
    for line in read_file.readlines():
        words = line.strip().split(" ")
        words_all += words
        unique_words = set(words_all)

    # print(words_all)

print(len(words_all))
print(len(unique_words))

with open("unique.text", mode="w") as write_file:
    for item in sorted(unique_words):
        write_file.write(item)
        # write_file.write(" ")
        write_file.write("\n")

print("finish")